/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  Image,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import React, { Component } from 'react'
import {createAppContainer} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Chat from './components/Chat'; 
import Save from './components/Save';
import Feed from './components/Feed';
import Home from './components/Home'; 
import Register from './components/Register';
import Login from './components/Login';
import Test from './components/Test';
import Icon from 'react-native-vector-icons/AntDesign';
import Icon1 from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/Entypo';
import Icon3 from 'react-native-vector-icons/Feather';
import { createBottomTabNavigator,createMaterialTopTabNavigator } from 'react-navigation-tabs';
import { createDrawerNavigator } from 'react-navigation-drawer';  

const stackHome = createStackNavigator({
  Home : Home,
  Login : Login,
  Register : Register

})
const stackLogin = createStackNavigator({
  Login : Login
})
const stackRegister = createStackNavigator({
  Register : Register
})



const appNavigation = createDrawerNavigator({
  ទំព័រដើម : stackHome,
  ចូលប្រព័ន្ធ : stackLogin,
  ចុះឈ្មោះ : stackRegister
  
})
export default createAppContainer(appNavigation);