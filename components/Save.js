import React, { Component } from 'react'
import { List, ListItem, Left, Body, Thumbnail, Text } from 'native-base';
import {SafeAreaView,ScrollView,FlatList} from 'react-native'
import {data} from './data';
export default class Save extends Component {
    ListSave = ({data}) => {
        return (
                <List>
                    <ListItem avatar>
                    <Left>
                        <Thumbnail source={{ uri: data.imageProfile }} />
                    </Left>
                    <Body>
                        <Text>{data.name}</Text>
                        <Text note numberOfLines={1}>{data.title}</Text>
                    </Body>
                    </ListItem>
                </List>
        )
    }
    render() {
        return (
            <SafeAreaView>
                <FlatList
                        data ={data}
                        renderItem={(data)=><this.ListSave data={data.item}></this.ListSave>}
                />
            </SafeAreaView>
        )
    }
}
