import React, { Component } from 'react'
import { List, ListItem, Left, Body, Thumbnail, Text } from 'native-base';
import {SafeAreaView,ScrollView,FlatList,Image,View} from 'react-native'
import {data} from './data';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon2 from 'react-native-vector-icons/Entypo';
export default class Home extends Component {
    static navigationOptions = ({ navigation }) => ({
        title: 'ទំព័រដើម',
        headerTitleStyle: { 
            textAlign:"center", 
            flex:1 
        },
        headerRight:
                <View style={{ paddingRight: 20 }}>
                    <Icon2 name="add-user" size={30} onPress={()=>navigation.navigate('Register')}></Icon2>
                </View>,
        headerLeft : 
                <View style={{ paddingLeft: 20 }}>
                    <Icon name="hamburger" size={30} onPress={()=>navigation.openDrawer()}></Icon>
                </View>
    });
    constructor(props){
        super(props);
        console.log(props)
    }
    render() {
        return (
            <SafeAreaView style={{flex:1,flexDirection:'column',justifyContent:'center'}}>
                <View>
                    <Image style={{width:120,height:100,alignSelf:'center'}} source={{uri:'https://images.vexels.com/media/users/3/151934/isolated/preview/9aa6fa876917b538540c5ea36f23c8e7-rabbit-animal-cartoon-by-vexels.png'}}></Image>
                    <Text style={{color:'teal',alignSelf:'center'}}>សូមស្វាគមន៏{':'+this.props.navigation.getParam('name','')}</Text>
                </View>
            </SafeAreaView>
        )
    }
}
