import React, { Component } from 'react'
import { List, ListItem, Left, Body, Thumbnail, Text, View ,Button} from 'native-base';
import {SafeAreaView,ScrollView,FlatList,Image,TextInput,Picker,AsyncStorage,TouchableOpacity} from 'react-native'
import RNPickerSelect from 'react-native-picker-select';
import {data} from './data';
import Icon from 'react-native-vector-icons/EvilIcons';
import { Dropdown } from 'react-native-material-dropdown';
export default class Register extends Component {
    static navigationOptions = ({ navigation }) => ({
        headerTitleStyle: { 
            textAlign:"center", 
            flex:1 
        },
        title: 'ចុះឈ្មោះ',
        headerLeft : 
            <TouchableOpacity style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}} onPress={ () => { navigation.goBack(null) } }>
                <Icon size={40}  name={'chevron-left'}  />
                <Text style={{fontSize:20}}>Back</Text>
            </TouchableOpacity>
    });
    constructor(props){
        super(props)
        this.state = {
            name : '',
            email : '',
            password : '',
            class : '', 
            error : {
                name : null,
                email : null,
                password : null,
                class : null
            }
        }
    }
    register = () => {
        if(this.checkName()){
           if(this.checkEmail()){
               if(this.checkClass()){
                    if(this.checkPassword()){
                        AsyncStorage.setItem('username',this.state.name)
                        AsyncStorage.setItem('password',this.state.password)
                        this.props.navigation.navigate('Login')
                    }
               }
           }
        }
        
    }
    checkName = () => {
        if(this.state.name.trim()==''){
            this.setState({
                error : {
                    name : '*ឈ្មោះមិនអាចទទេ'
                }
            })
            return false
        }else{
            let result = /^[a-zA-Z ]+$/.test(this.state.name);
            if(!result){
                this.setState({
                    error : {
                        name : '*ឈ្មោះមិនត្រឹមត្រូវ'
                    }
                })
                return false
            }else{
                if(this.state.name.trim().length>20){
                    this.setState({
                        error : {
                            name : '*ឈ្មោះមិនត្រូវលើសពី២០តួអក្សរ'
                        }
                    })
                    return false
                }else{
                    this.setState({
                        error : {
                            name : null
                        }
                    })
                    return true
                }
            }
        }
    }
    checkEmail = () => {
        if(this.state.email.trim()==''){
            this.setState({
                error : {
                    email : '*អុីម៊ែលមិនអាចទទេ'
                }
            })
            return false
        }else{
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var result = re.test(this.state.email)
            if(!result){
                this.setState({
                    error : {
                        email : '*អុីម៊ែលមិនត្រឹមត្រូវ'
                    }
                })
                return false
            }else
                this.setState({
                    error : {
                        email : null
                    }
                })
                return true
            }
    }
    checkClass = () => {
        if(this.state.class==''){
            this.setState({
                error:{
                    class : '*ថ្នាក់មិនអាចទទេ'
                }
            })
            return false
        }else{
            this.setState({
                error:{
                    class : null
                }
            })
            return true
        }
    }
    checkPassword = () => {
        if(this.state.password==''){
            this.setState({
                error:{
                    password : '*លេខសម្ងាត់មិនអាចទទេ'
                }
            })
            return false
        }else{
            if(this.state.password.length<7){
                this.setState({
                    error:{
                        password : '*លេខសម្ងាត់យ៉ាងហោចណាស់៧ខ្ទង់'
                    }
                })
                return false
            }else{
                this.setState({
                    error:{
                        password : null
                    }
                })
                return true
            }
        }
    }
    render() {
        return (
            <SafeAreaView style={{flex:1,flexDirection:'column',justifyContent:'center'}}>
                <View>
                    <Image style={{width:120,height:100,alignSelf:'center'}} source={{uri:'https://images.vexels.com/media/users/3/151934/isolated/preview/9aa6fa876917b538540c5ea36f23c8e7-rabbit-animal-cartoon-by-vexels.png'}}></Image>
                    <Text style={{color:'teal',alignSelf:'center'}}>សូមស្វាគមន៏មកកាន់ទន្សាយApp</Text>
                    <View style={{marginTop:20,alignItems:'center',marginLeft:10,marginRight:10}}>
                        <Text style={{color:'red',alignSelf:'flex-start'}}>{this.state.error.name}</Text>
                        <TextInput style={{width:'100%',height:50,backgroundColor:'#f0f2f2',padding:5,borderRadius:5}} onChangeText={text=>this.setState({name:text})} value={this.state.name}  placeholder={'ឈ្មោះ'}></TextInput>
                        <Text style={{color:'red',alignSelf:'flex-start'}}>{this.state.error.email}</Text>
                        <TextInput style={{width:'100%',height:50,backgroundColor:'#f0f2f2',padding:5,borderRadius:5}} onChangeText={text=>this.setState({email:text})} value={this.state.email} placeholder={'អុីម៊ែល'}></TextInput>
                        <Text style={{color:'red',alignSelf:'flex-start'}}>{this.state.error.class}</Text>
                        <Dropdown
                            containerStyle={{justifyContent:"flex-end",backgroundColor:'#f0f2f2',borderRadius:5,width:'100%',height:50}}
                            label='ថ្នាក់'
                            value={this.state.class}
                            onChangeText={text=>this.setState({class:text})}
                            data={[
                                {
                                    value : 'សៀមរាប'
                                },
                                {
                                    value : 'បាត់ដំបង'
                                }
                            ]}
                        />
                        <Text style={{color:'red',alignSelf:'flex-start'}}>{this.state.error.password}</Text>
                        <TextInput secureTextEntry={true} style={{width:'100%',height:50,backgroundColor:'#f0f2f2',padding:5,borderRadius:5}} value={this.state.password} onChangeText={text=>this.setState({password:text})} placeholder={'លេខសំងាត់'}></TextInput>
                        <Button onPress={()=>this.register()} style={{width:'100%',height:50,padding:5,marginTop:10,justifyContent:'center'}} rounded>
                            <Text>ចុះឈ្មោះ</Text>
                        </Button>
                        <View style={{flexDirection:'row'}}>
                            <Text style={{color:'balck',alignSelf:'center'}}>មិនទាន់មានគណនី?</Text>
                            <Text style={{color:'teal',alignSelf:'center'}} onPress={()=>this.props.navigation.navigate('Login')}>ចូលប្រើ</Text>
                        </View>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}
