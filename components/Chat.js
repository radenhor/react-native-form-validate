import React, { Component } from 'react'
import {Text,SafeAreaView} from 'react-native'
import { GiftedChat } from 'react-native-gifted-chat'
import SocketIO from 'socket.io-client';
import {onRecieve} from './../action/chatAction'
import {connect} from 'react-redux'
const chatAction = require('./../action/chatAction');
class Chat extends Component {
    static navigationOptions = {
        title: 'Chat',
    };
    constructor(props){
        super(props)
        this.state = {
            messages : []
        }
        this.id = Math.floor(Math.random() * 1000) + 1 
        this.socket = SocketIO('http://localhost:3000',{ path: '/test'});
        // this.socket.path('/test');
        this.socket.on('connect', () => {
           
        });
        
        this.socket.on('onSend',(data) => {
            // this.props.onRecieve(data.message)
            // console.log('hello')
            this.setState({
                messages: data.message,
            })
        })
        
    }

    componentDidMount(){
        
    }
    onSend(messages = []) {
        this.socket.emit('onSend',messages)
        
    }  
    render() {
        return (
                <GiftedChat
                    messages={this.state.messages}
                    onSend={messages => this.onSend(messages)}
                    user={{
                    _id: this.id,
                    }}
                />
        )
    }
}
const mapStateToProp = (s) => {
    return{
        data : s.messages
    }
}
export default connect(mapStateToProp,{onRecieve})(Chat)