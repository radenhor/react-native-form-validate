import React, { Component } from 'react'
import { Avatar } from 'react-native-elements';
import {SafeAreaView,View,Button,Image,TextInput,ScrollView,FlatList,Dimensions} from 'react-native'
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail, Text } from 'native-base';
import Icon from 'react-native-vector-icons/AntDesign';
import Icon1 from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/Entypo';
import Icon3 from 'react-native-vector-icons/Feather';
import RBSheet from "react-native-raw-bottom-sheet";
import {data} from './data';
export default class Feed extends Component {
    static navigationOptions = {
        title: 'Feed',
    };
    StoryCard = ({data}) => {
        console.log(data)
        return (
            data.index != 0 ?
            <View style={{marginRight:5}}>
                <View style={{position:'absolute',top:5,left:5,zIndex:1}}>
                                    <Avatar
                                        rounded
                                        source={{
                                            uri:data.item.imageProfile,
                                        }}
                                    />
                </View>
                <Text style={{position:'absolute',bottom:5,left:5,zIndex:1,color:'white',fontWeight:'bold'}}>{data.item.name}</Text>
                <Image style={{width:120,height:160,borderRadius:10}} resizeMode='cover' source={{uri:data.item.imagePost}}></Image>
            </View>
            :
            <View style={{marginRight:5}}>
                <View style={{position:'absolute',top:5,left:5,zIndex:1}}>
                                    <Avatar
                                        rounded
                                        source={{
                                            uri:'https://cdn4.iconfinder.com/data/icons/ios7-essence/22/add_plus-512.png',
                                        }}
                                    />
                </View>
                <Text style={{position:'absolute',bottom:5,left:5,zIndex:1,color:'white',fontWeight:'bold'}}>Add To Story</Text>
                <Image style={{width:120,height:160,borderRadius:10}} resizeMode='cover' source={{uri:'https://upload.wikimedia.org/wikipedia/commons/1/1f/Oryctolagus_cuniculus_Rcdo.jpg'}}></Image>
            </View>
        )
    }
    ListSave = ({data}) => {
        return (
                <List>
                    <ListItem avatar>
                    <Left>
                        <Thumbnail source={{ uri: data.imageProfile }} />
                    </Left>
                    <Body>
                        <Text>{data.name}</Text>
                        <Text note numberOfLines={1}>{data.title}</Text>
                    </Body>
                    </ListItem>
                </List>
        )
    }
    PostCard = ({data}) => {
        // let ratio=0;
        const SCREEN_WIDTH = Dimensions.get('window').width;
        // Image.getSize( data.imagePost, ( Width, Height ) => {    
        //     ratio = Height / Width;
        // });
        return(
                        <View style={{marginTop:10}}>
                            <View style={{paddingLeft:15,paddingRight:15}}>
                                <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                    <View style={{flexDirection:'row'}}>
                                        <Avatar
                                            rounded
                                            source={{
                                                uri:data.imageProfile,
                                            }}
                                            />
                                        <View style={{flexDirection:'column',margin:0,padding:0,justifyContent: 'center'}}>
                                        <Text style={{marginLeft:10,fontSize:13,fontWeight:'bold'}}>{data.name}</Text>
                                            <Text style={{marginLeft:10,fontSize:10,color:'gray'}}>{data.createdDate}</Text>
                                        </View>
                                    </View>
                                    <Icon3 name="more-horizontal" size={30} onPress={()=>this.RBSheet.open()}></Icon3>
                                </View>
                                <Text>{data.title}</Text>
                            </View>
                            <Image style={{width:SCREEN_WIDTH,height:SCREEN_WIDTH*1.2}} source={{uri:data.imagePost}}></Image>
                        </View>
        )
    }
    render() {
        return (
            <SafeAreaView>
                <ScrollView>
                    <View style={{paddingLeft:15,paddingRight:15,backgroundColor:'white',flexDirection:'row',justifyContent:'space-between'}}>
                        <Text style={{fontSize:30,color:'#1a7bf9',fontWeight:'bold'}}>facebook</Text>
                        <View style={{flexDirection:'row',alignItems:'center'}}>
                            <Icon name="search1" size={30} color='rgb(36,122,249)' />
                            <Icon name="message1" size={26} color='rgb(36,122,249)' />
                        </View>
                    </View>
                    <View style={{paddingLeft:15,paddingRight:15,flexDirection:'row'}}>
                        <Avatar
                            rounded
                            source={{
                                uri:
                                'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
                            }}
                            />
                        <View style={{flexDirection:'column',justifyContent: 'center'}}>
                            <TextInput style={{marginLeft:10,fontSize:17}} placeholder="What'on your mind?"></TextInput>
                        </View>
                    </View>
                    <View style={{paddingLeft:15,paddingRight:15,flexDirection:'row',justifyContent:'space-between',marginTop:10}}>
                        <View style={{flexDirection:'row'}}>
                            <Icon name="videocamera" color='rgb(36,122,249)' size={20}></Icon>
                            <View style={{justifyContent:'center',marginLeft:10}}>
                                <Text>Live</Text>
                            </View>
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <Icon1 name="photo" color='rgb(36,122,249)' size={20}></Icon1>
                            <View style={{justifyContent:'center',marginLeft:10}}>
                                <Text>Photo</Text>
                            </View>
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <Icon2 name="location" color='rgb(36,122,249)' size={20}></Icon2>
                            <View style={{justifyContent:'center',marginLeft:10}}>
                                <Text>Check In</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{paddingLeft:15,paddingRight:15,marginTop:10}}>
                        <FlatList
                                pagingEnabled={true}
                                horizontal={true}
                                data={data}
                                renderItem={(data)=><this.StoryCard data={data}></this.StoryCard>}
                                keyExtractor={item => item.id}
                        />   
                    </View>
                    <View>
                        <FlatList
                                    pagingEnabled={true}
                                    keyExtractor={Math.floor(Math.random()*10000)}
                                    horizontal={false}
                                    data={data}
                                    renderItem={(data)=><this.PostCard data={data.item}></this.PostCard>}
                                    keyExtractor={item => item.id}
                            />   
                    </View>
                    <RBSheet
                        ref={ref => {
                                       this.RBSheet = ref;
                                    }}
                                    height={400}
                                    duration={250}>
                                        <FlatList
                                            data ={data}
                                            renderItem={(data)=><this.ListSave data={data.item}></this.ListSave>}
                                            keyExtractor={item => item.id}
                                        />
                    </RBSheet>
                </ScrollView>
                
            </SafeAreaView>
        )
    }
}
