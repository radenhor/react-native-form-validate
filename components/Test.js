import React, { Component } from 'react'
import {SafeAreaView,Button,Text} from 'react-native'
import {data} from './data';
import { Form, TextValidator } from 'react-native-validator-form';
import { View } from 'native-base';
export default class Test extends Component {
    
    state = {
        email: '',
    }

    handleChange = (email) => {
        this.setState({ email });
    }

    submit = () => {
        // your submit logic
    }

    handleSubmit = () => {
        this.refs.form.submit();
    }

    render() {
        const { email } = this.state;
        return (
            <SafeAreaView>
            <Form
                ref="form"
                onSubmit={this.submit}
            >
                <View>
                    <TextValidator
                        style={{width: '100%',height: 50,borderRadius: 10,paddingLeft: 10,borderWidth:1,borderColor: 'black'}}
                        name="email"
                        label="email"
                        validators={['required', 'isEmail']}
                        errorMessages={['This field is required', 'Email invalid']}
                        placeholder="Your email"
                        type="text"
                        keyboardType="email-address"
                        value={email}
                        onChangeText={this.handleChange}
                    />
                </View>
                <Text>{this.props.navigation.state.params.user}</Text>
                 <Button
                    title="Submit1"
                    onPress={this.handleSubmit}
                />
            </Form>
            </SafeAreaView>
        
        );
    }
}
