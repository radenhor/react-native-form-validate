import React, { Component } from 'react'
import { List, ListItem, Left, Body, Thumbnail, Text,Button} from 'native-base';
import {SafeAreaView,ScrollView,FlatList,View,TextInput,Image,AsyncStorage,TouchableOpacity} from 'react-native'
import {data} from './data';
import Icon from 'react-native-vector-icons/EvilIcons';
export default class Login extends Component {
    static navigationOptions = ({ navigation }) => ({
        headerTitleStyle: { 
            textAlign:"center", 
            flex:1 
        },
        title: 'ចូលប្រព័ន្ធ',
        headerLeft : 
            <TouchableOpacity style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}} onPress={ () => { navigation.goBack(null) } }>
                <Icon size={40}  name={'chevron-left'}  />
                <Text style={{fontSize:20}}>Back</Text>
            </TouchableOpacity>
            
    });
    constructor(props){
        super(props);
        console.log(props)
        this.state = {
            name : '',
            password : '',
            error : {
                name : null,
                password : null
            }
        }
    }
    login = () => {
        if(this.checkName()){
            if(this.checkPassword()){
                AsyncStorage.getItem('username').then((name)=>{
                    AsyncStorage.getItem('password').then((password)=>{
                        console.log('state',this.state)
                        if(name == this.state.name && password == this.state.password){
                            console.log("name",name)
                            this.setState({
                                error : {
                                    name : null
                                }
                            })
                            this.props.navigation.navigate('Home',{name : this.state.name})
                        }
                        else{
                            this.setState({
                                error : {
                                    name : '*ឈ្មោះរីលេខសម្ងាត់មិនត្រឹមត្រូវ'
                                }
                            })
                        }
                    })
                })
                
            }
        }
    }
    checkName = () => {
        if(this.state.name==''){
            this.setState({
                error : {
                    name : '*ឈ្មោះមិនអាចទទេ'
                }
            })
            return false
        }else{
            this.setState({
                error : {
                    name : null
                }
            })
            return true
        }
    }
    checkPassword = () => {
        if(this.state.password==''){
            this.setState({
                error : {
                    password : '*លេខសម្ងាត់មិនអាចទទេ'
                }
            })
            return false
        }else{
            this.setState({
                error : {
                    password : null
                }
            })
            return true
        }
    }
    render() {
        return (
            <SafeAreaView style={{flex:1,flexDirection:'column',justifyContent:'center'}}>
                <View>
                    <Image style={{width:120,height:100,alignSelf:'center'}} source={{uri:'https://images.vexels.com/media/users/3/151934/isolated/preview/9aa6fa876917b538540c5ea36f23c8e7-rabbit-animal-cartoon-by-vexels.png'}}></Image>
                    <Text style={{color:'teal',alignSelf:'center'}}>សូមស្វាគមន៏មកកាន់ទន្សាយApp</Text>
                    <View style={{marginTop:20,alignItems:'center',marginLeft:10,marginRight:10}}>
                        <Text style={{color:'red',alignSelf:'flex-start'}}>{this.state.error.name}</Text>
                        <TextInput style={{width:'100%',height:50,backgroundColor:'#f0f2f2',padding:5,borderRadius:5}} value={this.state.name} onChangeText={text=>this.setState({name:text})} placeholder={'ឈ្មោះ'}></TextInput>
                        <Text style={{color:'red',alignSelf:'flex-start'}}>{this.state.error.password}</Text>
                        <TextInput secureTextEntry={true} style={{width:'100%',height:50,backgroundColor:'#f0f2f2',padding:5,borderRadius:5}} value={this.state.password} onChangeText={text=>this.setState({password:text})} placeholder={'លេខសំងាត់'}></TextInput>
                        <Button onPress={()=>this.login()} style={{width:'100%',height:50,padding:5,marginTop:10,justifyContent:'center'}} rounded>
                            <Text>ចូលប្រព័ន្ធ</Text>
                        </Button>
                        <View style={{flexDirection:'row'}}>
                            <Text style={{color:'balck',alignSelf:'center'}}>មានគណនីរួចហើយ?</Text>
                            <Text style={{color:'teal',alignSelf:'center'}} onPress={()=>this.props.navigation.navigate('Register')}>ចុះឈ្មោះទីនេះ</Text>
                        </View>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}
